# LesHouchesMP2024


# Mathematical Physics

Jul 29 to Aug 23 2024


---


- Week 1: 29 Jul > 02 Aug : 
**Mathematical Methods**

- Week 2: 05 > 09 Aug : 
**Quantum Gravity**

- Week 3: 12 > 16 Aug : 
**Gauge Theory**

- Week 4: 19 > 23 Aug : 
**Non-perturbative Physics**


---

# Organizers

- B. Eynard (IPhT)
- E. Garcia-Failde (U Sorbonne)
- A. Giacchetto (ETHZ)
- P. Gregori (IPhT)
- D. Lewanski (U Trieste)
- D. Mitsios




